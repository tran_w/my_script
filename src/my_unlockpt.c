/*
** my_unlockpt.c for  in /home/tran_w//Project/my_script/src
** 
** Made by duy-laurent tran
** Login   <tran_w@epitech.net>
** 
** Started on  Sun Mar  3 17:46:04 2013 duy-laurent tran
** Last update Sun Mar  3 19:27:13 2013 duy-laurent tran
*/

#include	<sys/ioctl.h>
#include	"my_script.h"

int		my_unlockpt(int fd)
{
  int		key;

  key = 0;
  return (ioctl(fd, TIOCSPTLCK, &key));
}
