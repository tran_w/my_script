/*
** my_login_tty.c for  in /home/tran_w//Project/my_script/src
** 
** Made by duy-laurent tran
** Login   <tran_w@epitech.net>
** 
** Started on  Sun Mar  3 17:41:37 2013 duy-laurent tran
** Last update Sun Mar  3 21:03:05 2013 duy-laurent tran
*/

#include		<unistd.h>
#include		"my_script.h"

int			my_login_tty(int fd)
{
  if (dup2(fd, 0) == R_FAILURE)
    return (R_FAILURE);  
  if (dup2(fd, 1) == R_FAILURE)
    return (R_FAILURE);
  if (dup2(fd, 2) == R_FAILURE)
    return (R_FAILURE);
  return (R_SUCCESS);
}
