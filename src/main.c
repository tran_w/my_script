/*
** main.c for  in /home/tran_w//Project/my_script/src
** 
** Made by duy-laurent tran
** Login   <tran_w@epitech.net>
** 
** Started on  Fri Mar  1 14:16:00 2013 duy-laurent tran
** Last update Sun Mar  3 22:12:23 2013 duy-laurent tran
*/

#include	<stdlib.h>
#include	<sys/types.h>
#include	<sys/stat.h>
#include	<fcntl.h>
#include	<sys/select.h>
#include	<sys/time.h>
#include	<sys/types.h>
#include	<unistd.h>
#include	<pty.h>
#include	"my_script.h"	

static int	if_fd_isset(char **buffer, int *aread, fd_set *readf, int fd_master)
{
  if (FD_ISSET(0, readf))
    {
      if ((*aread = read(0, *buffer, sizeof(**buffer))) < 0)
	return (R_FAILURE);
      if (*aread == 0)
	return (R_FAILURE);
      write(fd_master, *buffer, *aread);
    }
  else if (FD_ISSET(fd_master, readf))
    {
      if ((*aread = read(fd_master, *buffer, sizeof(**buffer))) < 0)
	return (R_FAILURE);
      if (*aread == 0)
	return (R_FAILURE);
      write(STDIN_FILENO, *buffer, *aread);
      return (1);
    }
  return (R_SUCCESS);
}

static int	get_filename_fd(t_script *k)
{
  int		fd;

  if (k->a_flag == 1)
    {
      if ((fd = open(k->filename, O_CREAT|O_WRONLY|O_APPEND, 0644)) == R_FAILURE)
	return (R_FAILURE);
    }
  else
    {
      if ((fd = open(k->filename, O_CREAT|O_WRONLY|O_TRUNC, 0644)) == R_FAILURE)
	return (R_FAILURE);
    }
  return (fd);
}

static void	loop(t_script *k, int fd_master)
{
  char		*buffer;
  int		aread; 
  int		ret;
  int		fd_file;
  fd_set	readf;

  aread = 0;
  if ((fd_file = get_filename_fd(k)) == R_FAILURE)
    return;
  if ((buffer = malloc(sizeof(*buffer) * 4096)) == NULL)
    return;
  while (42)
    {
      FD_ZERO(&readf);
      FD_SET(0, &readf);
      FD_SET(fd_master, &readf);
      if (select(fd_master + 1, &readf, NULL, NULL, NULL) == R_FAILURE)
	return;
      if ((ret = if_fd_isset(&buffer, &aread, &readf, fd_master)) == R_FAILURE)
	{
	  free(buffer);
	  return;
	}
      else if (ret == 1)
	write(fd_file, buffer, aread);
    }
  free(buffer);
}

int		main(int ac, char **av)
{
  t_script	k;
  struct termios raw;
  int		fd_master;
  pid_t		pid;

  parse_arguments(&k, ac, av);
  if ((pid = my_forkpty(&fd_master, NULL)) == R_FAILURE)
    return (R_FAILURE);
  if (pid == 0)
    execl("/bin/sh", "sh", 0, NULL);
  if (tcgetattr(0, &raw) == R_FAILURE)
    return (R_FAILURE);
  cfmakeraw(&raw);
  if (tcsetattr(0, TCSANOW, &raw) == R_FAILURE)
    return (R_FAILURE);
  loop(&k, fd_master);
  return (R_SUCCESS);
}
