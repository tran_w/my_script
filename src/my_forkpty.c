/*
** my_forkpty.c for  in /home/tran_w//Project/my_script/src
** 
** Made by duy-laurent tran
** Login   <tran_w@epitech.net>
** 
** Started on  Sun Mar  3 17:47:00 2013 duy-laurent tran
** Last update Sun Mar  3 20:42:09 2013 duy-laurent tran
*/

#include	<unistd.h>
#include	"my_script.h"

int             my_forkpty(int *amaster, char *name)
{
  pid_t		pid;
  int           fd;

  if (my_openpty(amaster, &fd, name) == R_FAILURE)
    return (R_FAILURE);
  if ((pid = fork()) == R_FAILURE)
    return (R_FAILURE);
  else if (pid == 0)
    {
      my_login_tty(fd);
      return (R_SUCCESS);
    }
  else
    {
      close(fd);
      return (pid);
    }
  return (R_FAILURE);
}
