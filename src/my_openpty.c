/*
** my_openpty.c for  in /home/tran_w//Project/my_script/src
** 
** Made by duy-laurent tran
** Login   <tran_w@epitech.net>
** 
** Started on  Sun Mar  3 17:45:56 2013 duy-laurent tran
** Last update Sun Mar  3 20:41:18 2013 duy-laurent tran
*/

#include		<stdlib.h>
#include		<sys/types.h>
#include		<sys/stat.h>
#include		<fcntl.h>
#include		"my_script.h"

int			my_openpty(int *amaster, int *aslave, char *name)
{
  if ((*amaster = open("/dev/ptmx", O_RDWR)) < 0)
    return (R_FAILURE);
  if (my_grantpt(*amaster) == R_FAILURE)
    return (R_FAILURE);
  if (my_unlockpt(*amaster) == R_FAILURE)
    return (R_FAILURE);
  if ((name = my_ptsname(*amaster)) == NULL)
    return (R_FAILURE);
  if ((*aslave = open(name, O_RDWR)) < 0)
    return (R_FAILURE);
  return (R_SUCCESS);
}
