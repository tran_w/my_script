/*
** my_grantpt.c for  in /home/tran_w//Project/my_script/src
** 
** Made by duy-laurent tran
** Login   <tran_w@epitech.net>
** 
** Started on  Sun Mar  3 16:38:54 2013 duy-laurent tran
** Last update Sun Mar  3 18:52:56 2013 duy-laurent tran
*/

#include	<stdlib.h>
#include	<unistd.h>
#include	<sys/types.h>
#include	<grp.h>
#include	<sys/stat.h>
#include	"my_script.h"

int		my_grantpt(int fd)
{
  struct group	*s_group;
  char		*slave_filename;
  
  if ((slave_filename = my_ptsname(fd)) == NULL)
    return (R_FAILURE);
  if ((s_group = getgrnam("tty")) == NULL)
    return (R_FAILURE);
  if ((chmod(slave_filename, 0620)) == R_FAILURE)
    return (R_FAILURE);
  if ((chown(slave_filename, getuid(), s_group->gr_gid)) == R_FAILURE)
    return (R_FAILURE);
  return (R_SUCCESS);
}
