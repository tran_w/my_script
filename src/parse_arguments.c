/*
** parseArguments.c for  in /home/tran_w//Project/my_script/src
** 
** Made by duy-laurent tran
** Login   <tran_w@epitech.net>
** 
** Started on  Fri Mar  1 14:45:15 2013 duy-laurent tran
** Last update Sun Mar  3 19:10:02 2013 duy-laurent tran
*/

#include		<string.h>
#include		<stdio.h>

#include		"my_script.h"

static void    		show_usage()
{
  printf("%s\n", USAGE_INFO);
}

static void		init_data(t_script *k)
{
  k->filename = DEFAULT_FILENAME;
  k->a_flag = 0;
}

void			parse_arguments(t_script *k, int ac, char **av)
{
  unsigned int		i;

  if (ac > 4)
    show_usage();
  else
    {
      i = 1;
      init_data(k);
      while (av[i])
	{
	  if (!strcmp("-a", av[i]))
	    k->a_flag = 1;
	  else
	    k->filename = av[i];
	  i++;
	}
    }
}
