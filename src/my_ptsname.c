/*
** my_ptsname.c for  in /home/tran_w//Project/my_script/src
** 
** Made by duy-laurent tran
** Login   <tran_w@epitech.net>
** 
** Started on  Sun Mar  3 17:46:00 2013 duy-laurent tran
** Last update Sun Mar  3 18:59:48 2013 duy-laurent tran
*/

#include		<stdlib.h>
#include		<stdio.h>
#include		<sys/ioctl.h>

char			*my_ptsname(int fd)
{
  unsigned int		n;
  static char	       	buffer[128];

  if (ioctl(fd, TIOCGPTN, &n) != 0)
    return (NULL);
  snprintf(buffer, sizeof(buffer), "/dev/pts/%u", n);
  return (buffer);
}
