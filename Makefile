##
## Makefile for  in /home/tran_w//Project/my_script
## 
## Made by duy-laurent tran
## Login   <tran_w@epitech.net>
## 
## Started on  Fri Mar  1 15:21:21 2013 duy-laurent tran
## Last update Sun Mar  3 19:28:18 2013 duy-laurent tran
##

NAME	=	my_script	

SRCS	=	src/main.c \
		src/parse_arguments.c \
		src/my_grantpt.c \
		src/my_login_tty.c \
		src/my_ptsname.c \
		src/my_unlockpt.c \
		src/my_forkpty.c \
		src/my_openpty.c

OBJS	=	$(SRCS:.c=.o)

CC	=	gcc -lutil

CFLAGS	+=	-W -Wall -I ./inc

all	:	$(NAME)

$(NAME)	:	$(OBJS)
		$(CC) -o $(NAME) $(OBJS)

clean	:	
		$(RM) $(OBJS)

fclean	:	clean
		$(RM) $(NAME)

re	:	fclean all

.PHONY	:	all fclean clean re
