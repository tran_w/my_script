/*
** my_script.h for  in /home/tran_w//Project/my_script/inc
** 
** Made by duy-laurent tran
** Login   <tran_w@epitech.net>
** 
** Started on  Fri Mar  1 15:00:38 2013 duy-laurent tran
** Last update Sun Mar  3 20:41:43 2013 duy-laurent tran
*/

#ifndef			MY_SCRIPT_H_
# define		MY_SCRIPT_H_

# define		USAGE_INFO		"./script [-a] [file]"
# define		DEFAULT_FILENAME	"typescript"

# define		R_SUCCESS		0
# define		R_FAILURE		-1

# include		<pty.h>

typedef struct		s_script
{
  unsigned int		a_flag;
  char			*filename;
}			t_script;

void			parse_arguments(t_script *k, int ac, char **av);
int			my_login_tty(int fd);		
int			my_grantpt(int fd);
int			my_unlockpt(int fd);
int			my_openpty(int *amaster, int *aslave, char *name); 
int			my_forkpty(int *amaster, char *name);
char			*my_ptsname(int fd);

#endif			/* !MY_SCRIPT_H_ */
